<h3>Dependencies</h3>
<b>SMO and DacFX</b><br>
Run the Powershell script [Install-NuGet_Dependencies.ps1] in a elevated Powershell Console<br>
<br>
OR<br>
Manual install<br>
First, you need to Setup the NuGet Package Provider<br>
1) Nuget<br>
In an Elevated Powershell session, type:<br>
<pre>
Register-PackageSource -Name Nuget -Location "http://www.nuget.org/api/v2" –ProviderName Nuget -Trusted
</pre>

2) Install DacFx DLLs<br>
In an Elevated Powershell session, type:<br>
<pre>
Install-Package Microsoft.SqlServer.DacFx.x64 -Source 'NuGet'
</pre>

3) Install SMO Dlls<br>
In an Elevated Powershell session, type:<br>
<pre>
Install-Package Microsoft.SqlServer.SqlManagementObjects -skipdependencies  -Source 'NuGet'
</pre>

4) Load and Verify the installed DLL Versions exist and can be loaded Powershell:<br>
<pre>Add-Type -Path 'C:\Program Files\PackageManagement\NuGet\Packages\Microsoft.SqlServer.SqlManagementObjects.160.2004021.0\lib\net462\Microsoft.SqlServer.Smo.dll'
Add-Type -Path 'C:\Program Files\PackageManagement\NuGet\Packages\Microsoft.SqlServer.DacFx.x86.150.4573.2\lib\net46\Microsoft.SqlServer.Dac.dll'</pre>

List Latest Nuget Package Versions:<br>
<pre>
Find-Package -Name "Microsoft.SqlServer.SqlManagementObjects" -Source "https://www.nuget.org/api/v2"
Find-Package -Name "Microsoft.SqlServer.DacFx.x64" -Source "https://www.nuget.org/api/v2"
</pre>

List your installed Package Versions:<br>
<pre>
get-package | where-object {$_.name -match 'SqlManagementObjects'} |sort version
get-package | where-object {$_.name -match 'DacFx'} |sort version
</pre>

<b>Analysis Services AMO<b><br>
https://docs.microsoft.com/en-us/azure/analysis-services/analysis-services-data-providers

<b>Active Directory</b>:<br>
If you want to resolve AD Group Members, you will need the RSAT (AD) Powershell Module.

RSAT for Windows 7<br>
https://www.microsoft.com/en-us/download/details.aspx?id=7887

RSAT for Windows 8.0<br>
https://www.microsoft.com/en-us/download/details.aspx?id=28972

RSAT for Windows 8.1<br>
https://www.microsoft.com/en-us/download/details.aspx?id=39296

RSAT for Windows 10<br>
Installing Windows 10 RSAT is more complicated:<br>
There is a separate download for each Windows 10 Build<br>
After the October 2018 Build, RSAT became a "Feature On Demand"<br>
Also, you will need to reinstall RSAT after a major Windows 10 Build upgrade!
